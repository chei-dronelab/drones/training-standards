# Scope

These are preliminary training standards that determine what levels of training are required to fly in certain environments/conditions.

All ratings will have a minimum number of curriculum hours, training flight hours, and training takeoffs/landings to achieve that rating.  All ratings will also have a minimum number of annual hours and/or takeoffs/landings to maintain that rating.

All pilots must also hold a type rating for the specific aircraft they are operating.  Test Pilot rating may operate any aircraft matching their training.

Payload Operators must also hold a type rating for the specific payload they are operating.

# Basic Autonomous Multirotor Pilot
This rating trains the pilot to operate small unmanned multirotors no heavier than 20 lbs MTOW where there are no static obstacles within the operating hemisphere in position controlled modes in accordance with Part 107 rules.

# Night Rating
This rating trains the pilot to operate unmanned aerial vehicles at night in accordance with Part 107 rules.

# Manual Multirotor Rating
This rating trains the pilot to operate unmanned multirotors in non-GPS controlled modes.

# Low-Level Multirotor Rating
This rating trains the pilot to operate unmanned multirotors where there are static obstacles within the operating hemisphere that do not overhang the aircraft.

Prerequisites: Manual Rating

# BVR Rating
This rating trains the pilot to operate unmanned aerial vehicles beyond visul range in accordance with Part 107 rules.

# Large Multirotor Rating
This rating trains the pilot to operate small unmanned multirotors heavier than 20 lbs MTOW.

# Aerial Photogrammetry Rating
This rating trains the pilot to conduct aerial photogrammetry missions.

# Restricted Airspace Rating
This rating trains the pilot to operate in restricted airspace in accordance with Part 107 rules.

# Overhead Multirotor Rating
This rating trains the pilot to operate unmanned multirotors where obstacles overhang the aircraft.

Prerequisites: Low-Level Rating

# Test Pilot Rating, Multirotor
This rating trains the pilot to operate experimental multirotors.

Prerequisites: Manual Rating

# Maritime Multirotor Pilot Rating
This rating trains the pilot to operate small unmanned multirotors from a maritime vehicle context.

Prerequisites: Manual Rating

# Multirotor Instructor
This rating trains the pilot to instruct other pilots

Prerequisites: Manual Rating

# Multirotor Instructor Trainer
This rating trains the instructor to instruct other instructors.

Prerequisites: Instructor

# Type Ratings
## DJI M300
## Phantom 4 Pro
## Phantom 4 Pro RTC
## Alta X
## DJI Inspire
## DJI M600
## UGCS
## Mission Planner
## QGroundControl

# Payload Ratings
## Headwall Co-Aligned Hyperspectral Camera
